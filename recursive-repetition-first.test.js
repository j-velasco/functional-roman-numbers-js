const roman = (n, acc = '') => {
    if (n === 0) return acc

    const descendingRomanNumbersTranslation = [
        [1000, 'M'],
        [900, 'CM'],
        [500, 'D'],
        [400, 'CD'],
        [90, 'XC'],
        [50, 'L'],
        [40, 'XL'],
        [10, 'X'],
        [9, 'IX'],
        [5, 'V'],
        [4, 'IV'],
        [1, 'I'],
    ]
    const translation = descendingRomanNumbersTranslation.find(t => t[0] <= n)

    const repetitions = Math.floor(n / translation[0])
    return roman(n - repetitions * translation[0], acc + times(repetitions, translation[1]))
}

describe('recursive version, starting with repetition', () => {
    it('translates 1 to I', () => {
        expect(roman(1)).toBe('I')
    })

    it('repeats I', () => {
        expect(roman(2)).toBe('II')
        expect(roman(3)).toBe('III')
    })

    it('translates 5 to V', () => {
        expect(roman(5)).toBe('V')
    })

    it('translates 6 to VI', () => {
        expect(roman(6)).toBe('VI')
    })

    it('translates 4 to IV', () => {
        expect(roman(4)).toBe('IV')
    })

    it('translates all pending mappings', () => {
        expect(roman(9)).toBe('IX')
        expect(roman(10)).toBe('X')
        expect(roman(40)).toBe('XL')
        expect(roman(50)).toBe('L')
        expect(roman(90)).toBe('XC')
        expect(roman(400)).toBe('CD')
        expect(roman(500)).toBe('D')
        expect(roman(900)).toBe('CM')
        expect(roman(1000)).toBe('M')
    })

    it('works with user acceptance tests', () => {
        expect(roman(2018)).toBe('MMXVIII')
        expect(roman(1983)).toBe('MCMLXXXIII')
        expect(roman(1977)).toBe('MCMLXXVII')
        expect(roman(100000)).toBe(times(100, 'M'))
    })

    it('works with big numbers', () => {
        expect(roman(100000000)).toBe(times(100000, 'M'))
    })
})

const times = (number, symbol) => new Array(number).fill(symbol).join('')
