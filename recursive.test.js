const arabicToRomanSymbolMap = {
    1: 'I',
    5: 'V',
    10: 'X',
    50: 'L',
    100: 'C',
    500: 'D',
    1000: 'M',
}
const numbersWithRomanSymbol = Object.keys(arabicToRomanSymbolMap).map(k => parseInt(k))
const descendingRomans = [...numbersWithRomanSymbol].sort((a, b) => b - a) // eslint-disable-line fp/no-mutating-methods
const ascendingRomans = [...numbersWithRomanSymbol].sort((a, b) => a - b) // eslint-disable-line fp/no-mutating-methods

const firstRomanLessOrEqualToNumber = number => descendingRomans.find(i => i <= number)
const firstRomanBiggerThanNumber = number => ascendingRomans.find(i => i > number)

const subtractiveNotation = number => {
    const romanNumberAfterNumber = firstRomanBiggerThanNumber(number)
    const minuendForSubtraction = {
        5: 1,
        10: 1,
        50: 10,
        100: 10,
        500: 100,
        1000: 100,
    }[romanNumberAfterNumber]

    const minuend = romanNumberAfterNumber - minuendForSubtraction
    if (!minuendForSubtraction || minuend > number) {
        return
    }

    return {
        minuend,
        symbol: arabicToRomanSymbolMap[minuendForSubtraction] + arabicToRomanSymbolMap[romanNumberAfterNumber],
    }
}

const repeatBiggerNotation = number => {
    const romanNumberBefore = firstRomanLessOrEqualToNumber(number)
    const nbRepetitions = Math.floor(number / romanNumberBefore)

    return {
        minuend: romanNumberBefore * nbRepetitions,
        symbol: times(nbRepetitions, arabicToRomanSymbolMap[romanNumberBefore]),
    }
}

const roman = (number, accRoman = '') => {
    if (number === 0)
        return accRoman
    if (arabicToRomanSymbolMap[number])
        return accRoman + arabicToRomanSymbolMap[number]

    const pair = subtractiveNotation(number) || repeatBiggerNotation(number)
    return roman(number - pair.minuend, accRoman + pair.symbol)
}

const times = (number, symbol) => new Array(number).fill(symbol).join('')

describe('recursive version', () => {
    it('knows all roman symbols', () => {
        expect(roman(1)).toBe('I')
        expect(roman(5)).toBe('V')
        expect(roman(10)).toBe('X')
        expect(roman(50)).toBe('L')
        expect(roman(100)).toBe('C')
        expect(roman(500)).toBe('D')
        expect(roman(1000)).toBe('M')
    })

    it('repeats necessaries Ms', () => {
        expect(roman(4000)).toBe('MMMM')
        expect(roman(2100)).toBe('MMC')
    })

    it('repeats Cs', () => {
        expect(roman(300)).toBe('CCC')
    })

    it('repeats roman symbol for bigger divisor', () => {
        expect(roman(20)).toBe('XX')
    })

    it('uses subtractive notation', () => {
        expect(roman(4)).toBe('IV')
        expect(roman(9)).toBe('IX')
        expect(roman(40)).toBe('XL')
        expect(roman(90)).toBe('XC')
        expect(roman(400)).toBe('CD')
        expect(roman(900)).toBe('CM')
    })

    it('works with user acceptance tests', () => {
        expect(roman(2018)).toBe('MMXVIII')
        expect(roman(1983)).toBe('MCMLXXXIII')
        expect(roman(1977)).toBe('MCMLXXVII')
        expect(roman(100000)).toBe(times(100, 'M'))
    })

    it('works with big numbers', () => {
        expect(roman(100000000)).toBe(times(100000, 'M'))
    })
})
